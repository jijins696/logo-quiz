//
//  QuizPageViewController.m
//  Logo Quiz
//
//  Created by Jijin Sundar on 20/11/21.
//

#import "QuizPageViewController.h"

@interface QuizPageViewController ()

@property(strong, nonatomic) NSDictionary *imageDict;
@property(strong, nonatomic) UITextField *answerTextField;
@property(strong, nonatomic) UILabel *scoreLbl;
@property(strong, nonatomic) UIImageView *imageView


@end

@implementation QuizPageViewController{

int score;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureQuizPage];
}

-(void)configureQuizPage{
    [self.view setBackgroundColor:[UIColor colorWithRed:130.0f/255.0f green:173.0f/255.0f blue:127.0f/255.0f alpha:1.0]];
    _imageView = [[UIImageView alloc] init];
    [self loadQuestion];
    [self configureAnswerTextField];
    [self configureSubmitButton];
    [self configureScoreLabel];
    score = 0;
}


-(void)loadQuestion{
    int index = arc4random_uniform(6);
    _imageDict = [_logolist objectAtIndex:index];
    NSString *imageURL = [_imageDict valueForKey:@"imgUrl"];
    [self configurequestionImageView:imageURL];
}

-(void)configurequestionImageView:(NSString *)imageURL{
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: imageURL]];
    UIImage *imageViews = [UIImage imageWithData: imageData];
    [_imageView setImage:imageViews];
    [self.view addSubview:_imageView];
    [_imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:100];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:100];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
    
    [self.view addConstraints:@[left, top]];
    [imageView addConstraints:@[height, width]];
    
}

-(void)configureAnswerTextField{
    
    _answerTextField = [[UITextField alloc] init];
    [self.view addSubview:_answerTextField];
    [_answerTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_answerTextField setBackgroundColor:[UIColor whiteColor]];
    _answerTextField.delegate = self;
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_answerTextField attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:50];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_answerTextField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:400];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:_answerTextField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:_answerTextField attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:300];
    
    [self.view addConstraints:@[left, top]];
    [_answerTextField addConstraints:@[height, width]];
    
}

-(void)configureSubmitButton{
    
    UIButton *submitBtn = [[UIButton alloc] init];
    [self.view addSubview:submitBtn];
    [submitBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [submitBtn setBackgroundColor:[UIColor grayColor]];
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    
    [submitBtn addTarget:self action:@selector(submitButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:80];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:500];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:submitBtn attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:240];
    
    [self.view addConstraints:@[left, top]];
    [submitBtn addConstraints:@[height, width]];
}

-(void)configureScoreLabel{
    
    _scoreLbl = [[UILabel alloc] init];
    [self.view addSubview:_scoreLbl];
    [_scoreLbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    _scoreLbl.text = @"score : 0";
    _scoreLbl.textColor = [UIColor blackColor];
    _scoreLbl.textAlignment =  NSTextAlignmentCenter;
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:_scoreLbl attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:80];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_scoreLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:10];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:_scoreLbl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:_scoreLbl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:240];
    
    [self.view addConstraints:@[left, top]];
    [_scoreLbl addConstraints:@[height, width]];
}

-(void)submitButtonAction{
    NSString *answer = [_imageDict objectForKey:@"name"];
    if( [answer caseInsensitiveCompare:_answerTextField.text] == NSOrderedSame ) {
        score ++;
    }
    _scoreLbl.text = [NSString stringWithFormat:@"Score : %d",score];
    [self loadQuestion];
}



@end
