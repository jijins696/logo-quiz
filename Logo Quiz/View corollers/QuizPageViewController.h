//
//  QuizPageViewController.h
//  Logo Quiz
//
//  Created by Jijin Sundar on 20/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuizPageViewController : UIViewController

@property(nonatomic,strong)NSArray *logolist;

@end

NS_ASSUME_NONNULL_END
