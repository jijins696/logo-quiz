//
//  ViewController.m
//  Logo Quiz
//
//  Created by Jijin Sundar on 20/11/21.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithRed:0.97 green:0.43 blue:0.43 alpha:1.0]];
    UILabel *welocmeLabel = [[UILabel alloc] init];
    [self.view addSubview:welocmeLabel];
    [welocmeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [welocmeLabel setText:@"Welcome"];
    
    
}

- (IBAction)quizStartActionPerformed:(id)sender {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"json"];
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];

    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    QuizPageViewController *quizViewController = [[QuizPageViewController alloc] init];
    quizViewController.logolist = [jsonDict objectForKey:@"logos"];
    [self presentViewController:quizViewController animated:YES completion:nil];
    
}



@end
